var mongoose=require('mongoose');

var uristring =
    process.env.MONGODB_URI ||
    'mongodb://localhost:27017/newsfeed';
console.log('MongoDB URL ' + uristring);
 mongoose.connect(uristring, function (err, res) {
      if (err) {
      console.log ('ERROR connecting to: ' + uristring + '. ' + err);
      } else {
      console.log ('Succeeded connected to: ' + uristring);
      }
    });

var NewsFeedSchema = new mongoose.Schema({
        category: { type: String, required: true, trim: true },
	title: { type: String, required: true, trim: true },
        description: { type: String, required: true, trim: true },
        link: { type: String, required: true},
        published: { type: Boolean, required: true, default: false },
        createdAt: { type: Number },
        updatedAt: { type: Number }
});
NewsFeedSchema.pre('save',function(next) {
    this.updatedAt = Date.now();
    if (this.isNew) this.createdAt = this.updatedAt;
    next();
});

var newsFeedModel = mongoose.model('NewsFeedSchema', NewsFeedSchema);
var exports = module.exports = {};
exports.saveFeed = function(feedObj){
          var newsFeed = new newsFeedModel(
          {category:feedObj['category'],
           title:feedObj['title'],
           description:feedObj['description'],
           link:feedObj['link']
}
          );
       newsFeed.save(function (err) {if (err) console.log (err)});      
};

exports.listFeed = function(){newsFeedModel.find({}).exec(function(err, result) {
      if (!err) {
        console.log(result);
      } else {
        console.log(error);
      };
    });
};


