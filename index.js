var Feed = require('rss-to-json');
var db=require('./db');
var sqs=require('./sqs');
fs = require('fs');

var processExit=function(){
    console.log("Successfully send latest news");
    process.exit(0);
}
var storeFeeds=function(err,feedObj,cb){
          console.log('Saving records to database..');
          db.saveFeed(feedObj);
          console.log('sending message');
          sqs.sendMessage(feedObj);
          cb();
}

var processFeed = function(err,name,rss,cb){
        var feedObj={};
        for(var i=0;i<rss.items.length;++i){
             for(key in rss.items[i]){
                  
                  if(key.toString()=='title'){
                     feedObj.title=rss.items[i][key];
                  }else if(key.toString()=='description'){
                         feedObj.description=rss.items[i][key];
                  }else if(key.toString()=='link'){
                         feedObj.link=rss.items[i][key];
                  }
          
             }
             feedObj.category=name;
             cb(err,feedObj,processExit);
	}
}

var parseFeed = function(name,feedLink,callback){Feed.load(feedLink, function(err, rss){
    if (err) {
    return console.log(err);
  }
    console.log("Fetched news feeds successfully");
    callback(err,name,rss,storeFeeds);
});
};

//exports.handler=function(event,context,callback){feedsLinkArray.forEach(function(entry){
    //parseFeed(entry,processFeed);
    //callback();
//});
//};
fs.readFile('/news-sources/links.txt', 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  console.log('Read input file successfully');
  var result=JSON.parse(data);
  result.categories.forEach(function(category){
       category.links.forEach(function(link){
         parseFeed(category.name,link,processFeed);
}

);
}
);

});


//feedsLinkArray.forEach(function(entry){

    
    //parseFeed(entry,processFeed);
   
//});
